
package dev.delle.tim.semanticweb.data.schools;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Lat {

    @Expose
    private String type;
    @Expose
    private String datatype;
    @Expose
    private String value;

    /**
     * 
     * @return
     *     The type
     */
    public String getType() {
        return type;
    }

    /**
     * 
     * @param type
     *     The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * 
     * @return
     *     The datatype
     */
    public String getDatatype() {
        return datatype;
    }

    /**
     * 
     * @param datatype
     *     The datatype
     */
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }

    /**
     * 
     * @return
     *     The value
     */
    public String getValue() {
        return value;
    }

    /**
     * 
     * @param value
     *     The value
     */
    public void setValue(String value) {
        this.value = value;
    }

}
