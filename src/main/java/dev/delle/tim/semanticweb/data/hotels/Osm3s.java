
package dev.delle.tim.semanticweb.data.hotels;

import java.util.List;

public class Osm3s{
   	private String copyright;
   	private String timestamp_areas_base;
   	private String timestamp_osm_base;

 	public String getCopyright(){
		return this.copyright;
	}
	public void setCopyright(String copyright){
		this.copyright = copyright;
	}
 	public String getTimestamp_areas_base(){
		return this.timestamp_areas_base;
	}
	public void setTimestamp_areas_base(String timestamp_areas_base){
		this.timestamp_areas_base = timestamp_areas_base;
	}
 	public String getTimestamp_osm_base(){
		return this.timestamp_osm_base;
	}
	public void setTimestamp_osm_base(String timestamp_osm_base){
		this.timestamp_osm_base = timestamp_osm_base;
	}
}
