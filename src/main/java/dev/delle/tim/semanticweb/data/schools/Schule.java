
package dev.delle.tim.semanticweb.data.schools;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Schule {

    @Expose
    private School school;
    @Expose
    private Label label;
    @Expose
    private Alabel alabel;
    @Expose
    private Address address;
    @Expose
    private Lat lat;
    @SerializedName("long")
    @Expose
    private Long _long;

    /**
     * 
     * @return
     *     The school
     */
    public School getSchool() {
        return school;
    }

    /**
     * 
     * @param school
     *     The school
     */
    public void setSchool(School school) {
        this.school = school;
    }

    /**
     * 
     * @return
     *     The label
     */
    public Label getLabel() {
        return label;
    }

    /**
     * 
     * @param label
     *     The label
     */
    public void setLabel(Label label) {
        this.label = label;
    }

    /**
     * 
     * @return
     *     The alabel
     */
    public Alabel getAlabel() {
        return alabel;
    }

    /**
     * 
     * @param alabel
     *     The alabel
     */
    public void setAlabel(Alabel alabel) {
        this.alabel = alabel;
    }

    /**
     * 
     * @return
     *     The address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * 
     * @param address
     *     The address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * 
     * @return
     *     The lat
     */
    public Lat getLat() {
        return lat;
    }

    /**
     * 
     * @param lat
     *     The lat
     */
    public void setLat(Lat lat) {
        this.lat = lat;
    }

    /**
     * 
     * @return
     *     The _long
     */
    public Long getLong() {
        return _long;
    }

    /**
     * 
     * @param _long
     *     The long
     */
    public void setLong(Long _long) {
        this._long = _long;
    }

}
