
package dev.delle.tim.semanticweb.data.hotels;

import java.util.List;

public class Hotels{
   	private List<Elements> elements;
   	private String generator;
   	private Osm3s osm3s;
   	private Number version;

 	public List<Elements> getElements(){
		return this.elements;
	}
	public void setElements(List<Elements> elements){
		this.elements = elements;
	}
 	public String getGenerator(){
		return this.generator;
	}
	public void setGenerator(String generator){
		this.generator = generator;
	}
 	public Osm3s getOsm3s(){
		return this.osm3s;
	}
	public void setOsm3s(Osm3s osm3s){
		this.osm3s = osm3s;
	}
 	public Number getVersion(){
		return this.version;
	}
	public void setVersion(Number version){
		this.version = version;
	}
}
