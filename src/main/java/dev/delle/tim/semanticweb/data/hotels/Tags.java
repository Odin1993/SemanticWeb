
package dev.delle.tim.semanticweb.data.hotels;

import com.google.gson.annotations.SerializedName;

public class Tags{

	@SerializedName("addr:city")
   	private String addrCity;

	@SerializedName("addr:postcode")
   	private String addrPostcode;

	@SerializedName("contact:phone")
   	private String contactPhone;

	@SerializedName("contact:website")
   	private String contactWebsite;

   	private String name;
   	private String tourism;

	public String getAddrCity() {
		return addrCity;
	}

	public void setAddrCity(String addrCity) {
		this.addrCity = addrCity;
	}

	public String getAddrPostcode() {
		return addrPostcode;
	}

	public void setAddrPostcode(String addrPostcode) {
		this.addrPostcode = addrPostcode;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public String getContactWebsite() {
		return contactWebsite;
	}

	public void setContactWebsite(String contactWebsite) {
		this.contactWebsite = contactWebsite;
	}

	public String getName(){
		return this.name;
	}
	public void setName(String name){
		this.name = name;
	}
 	public String getTourism(){
		return this.tourism;
	}
	public void setTourism(String tourism){
		this.tourism = tourism;
	}
}
