package dev.delle.tim.semanticweb.data.playgrounds;

/**
 * Created by lyco on 30/05/15.
 */
public class Playgrounds {
    private Playground[] playgrounds;

    public Playgrounds(Playground[] playgrounds) {
        this.playgrounds = playgrounds;
    }

    public Playground[] getPlaygrounds() {
        return playgrounds;
    }

    public void setPlaygrounds(Playground[] playgrounds) {
        this.playgrounds = playgrounds;
    }
}

